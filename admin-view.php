<?php
    $args = array(
        'public'   => true,
        '_builtin' => false
    );

    $post_types = get_post_types( $args );
    array_push( $post_types, 'post' );

    $update_message = '';

    if( count( $_POST ) > 0 && isset( $_POST['template_settings'] ) ) {

        if( !empty( $_POST['post_type_name'] ) ) {

            $impVal = implode( ',', $_POST['post_type_name'] );
            delete_option ( 'jold_cpt_template' );
            add_option ( 'jold_cpt_template', $impVal );
            $update_message = __( 'Setting Saved.', 'jold_cpt_templates' ) ;

        } else {

            delete_option ( 'jold_cpt_template' );
            add_option ( 'jold_cpt_template', 'post' );
            $update_message = __( 'Please select atleast one post type.', 'jold_cpt_templates' );

        }
    }

    if( isset( $_REQUEST['template_reset'] ) && $_REQUEST['template_reset'] == 'reset' ) {

        $impVal = $_POST['post_type_name'] = '';
        delete_option ( 'jold_cpt_template' );
        add_option ( 'jold_cpt_template','post' );
        $update_message = __( 'Default Setting Saved.', 'jold_cpt_templates' );
    }
?>


<div class="wrap">

    <h1><?php _e('WP Custom Post Template'); ?></h1>

    <?php if ($_POST) : ?>
        <div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible">
            <p>
                <?php echo $update_message; ?>
            </p>
        </div>
    <?php endif; ?>

    <form action="" method="post" enctype="multipart/form-data">
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row">
                        <?php _e( 'Enable on post types:', '' ); ?>
                    </th>
                    <td>
                        <fieldset>
<?php
    $counter = "";
    $chd     = "";

    foreach($post_types as $type){

        $counter++;
        $obj = get_post_type_object( $type );
        $post_types_name = $obj->labels->singular_name;

        if( get_option( 'jold_cpt_template' ) != ''){

            $postType_title = get_option( 'jold_cpt_template' );
            $postType_chkd = explode( ',', $postType_title );
            $chd = '';
            if( in_array( $type, $postType_chkd ) ) {
                $chd = 'checked="checked"';
            }

        }
?>
                            <label for="<?php echo $type; ?>">
                            <input name="post_type_name[]" type="checkbox" value="<?php echo $type; ?>" id="<?php echo $type; ?>" <?php echo $chd; ?> />
                                <?php echo $post_types_name; ?>
                            </label>
                            <br />
<?php
    }
?>
                            <p class="description"><?php _e( 'Note: If you are using default setting then post template will show only on default post.', 'jold_cpt_templates' ); ?></p>
                            <p class="submit">
                                <input type="submit" name="submit" value="<?php _e('Save Settings', 'jold_cpt_templates'); ?>" class="button button-primary" />
                                <input type="hidden" name="template_settings" value="save" style="display:none;" />
                            </p>
                        </fieldset>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>


    <form action="" method="post" enctype="multipart/form-data">
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row">
                        <?php _e( 'Reset to default settings:', '' ); ?>
                    </th>
                    <td>
                        <fieldset>
                            <p class="description"><?php _e( 'Note: If you are using default setting then post template will show only on default post.', 'jold_cpt_templates' ); ?></p>
                            <p class="submit">
                                <input type="submit" name="submit" value="<?php _e('Default Settings', 'jold_cpt_templates'); ?>" class="button button-primary" />
                                <input type="hidden" name="template_reset" value="reset" style="display:none;" />
                            </p>
                        </fieldset>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>

</div>
