<?php
/*
    Plugin Name:            Custom Posttype Templates
    Description:            Allows for custom post type templates as you would with default pages in WordPress.
    Version:                1.2.1
    Plugin URI:             https://bitbucket.org/joldnl/custom-posttype-templates
    Bitbucket Plugin URI:   https://bitbucket.org/joldnl/custom-posttype-templates
    Author:                 Jurgen Oldenburg
    Author URI:             http://www.jold.nl
    Text Domain:            jold-acfadd
*/


add_action( 'add_meta_boxes',   'jold_cpt_template_add' );
add_action( 'save_post',        'jold_cpt_templates_save', 10, 2 );
add_filter( 'single_template',  'jold_cpt_templates_loader' );
// add_action( 'add_meta_boxes',   'jold_cpt_template_add' );



/**
 * Register plugin install hook
 * @return none
 */
function jold_cpt_template_activate() {

	add_option( 'jold_cpt_template', 'post' ) ;

}
register_activation_hook( __FILE__, 'jold_cpt_template_activate' );




/**
 * Register plugin deactivation hook
 * @return none
 */
function jold_cpt_template_deactivate() {

	// Delete otion when plugin deactivated
	delete_option( 'jold_cpt_template' );

}
register_deactivation_hook( __FILE__, 'jold_cpt_template_deactivate' );




/**
 * Register "WP Post Template" admin menu item under "Settings"
 * @return none
 */
add_action( 'admin_menu', 'wp_post_template_admin_menu' );
function wp_post_template_admin_menu() {
    if ( is_admin() ) {
        add_submenu_page(
            'options-general.php',
            'WP Post Template',
            'WP Post Template',
            'manage_options',
            'cpt-templates',
            'jold_cpt_template_admin_view'
        );
	}
}




/**
 * Register admin view function
 * @return none
 */
function jold_cpt_template_admin_view(){
	include('admin-view.php');
}




/**
 * Register the admin metabox
 * @param string    The post type.
 */
function jold_cpt_template_add( $postType ) {

    // get option value
	if( get_option('jold_cpt_template') == '' ){

		$postType_title = 'post';
		$postType_arr[] = $postType_title;

	} else {

		$postType_title = get_option( 'jold_cpt_template' );
		$postType_arr = explode( ',', $postType_title );

	}
	if( in_array( $postType, $postType_arr ) ) {
		add_meta_box(
			'postparentdiv',
			__( 'Custom Post Template', 'jold_cpt_template' ),
			'jold_cpt_template_meta_box',
			$postType,
			'side',
			'core'
		);
	}

}




/**
 * Register the post metabox.
 * @param  object    $post The current post object.
 * @return html      The contents of the metbox.
 */
function jold_cpt_template_meta_box( $post ) {

	if ( $post->post_type != 'page' && 0 != count( jold_cpt_templates_get() ) ) {
		$template = get_post_meta($post->ID,'_post_template',true);
?>
    <p><strong><?php _e( 'Select template', 'jold_cpt_template' ); ?></strong></p>
	<label class="screen-reader-text" for="post_template"><?php _e( 'Post Template', 'jold_cpt_template' ) ?></label>
	<select name="post_template" id="post_template" style="width: 100%;">
		<option value='default'><?php _e( 'Default Template', 'jold_cpt_template' ); ?></option>
		<?php jold_cpt_template_dropdown( $template ); ?>
	</select>
	<p><i><?php _e( 'Some themes have custom templates you can use for single posts template selecting from dropdown.', 'jold_cpt_template' ); ?></i></p>

<?php
	}
}




/**
 * Populate the postbox dropdown with all available custom templates.
 * @param  string $default   The default template file.
 * @return
 */
function jold_cpt_template_dropdown( $default = '' ) {

    $templates = jold_cpt_templates_get();
    ksort( $templates );

    foreach (array_keys( $templates ) as $template ) {
        if ( $default == $templates[$template] ) {
            $selected = " selected='selected'";
        } else {
            $selected = '';
        }
        echo "\n\t<option value='".$templates[$template]."' $selected>$template</option>";
    }

}




/**
 * Get the custom template.
 * @return array  The details of the selected template.
 */
function jold_cpt_templates_get() {

    if( function_exists('wp_get_themes') ) {
        $themes = wp_get_themes();
    } else {
        $themes = get_themes();
    }

    $theme          = get_option( 'template' );
    $templates      = $themes[$theme]['Template Files'];
    $post_templates = array();

    if ( is_array( $templates ) ) {
        $base = array( trailingslashit( get_template_directory() ), trailingslashit( get_stylesheet_directory() ) );

        foreach ( $templates as $template ) {

            $basename = str_replace( $base, '', $template );

            if ( $basename != 'functions.php' ) {

                // don't allow template files in subdirectories
                // if ( false !== strpos($basename, '/') ) {
                //     continue;
                // }

                $template_data = implode( '', file( $template ));

                $name = '';

                if ( preg_match( '| Post Template:(.*)$|mi', $template_data, $name ) ) {
                    $name = _cleanup_header_comment($name[1]);
                }

                if ( !empty( $name ) ) {
                    $post_templates[trim( $name )] = $basename;
                }

            }
        }
    }

    return $post_templates;
}




/**
 * Save the custom template to the post meta data.
 * @param  int     $post_id   The unique post id.
 * @param  object  $post      The current post object.
 * @return none
 */
function jold_cpt_templates_save( $post_id, $post ) {

    if ( $post->post_type !='page' && !empty( $_POST['post_template'] ) ) {
        update_post_meta( $post->ID, '_post_template', $_POST['post_template'] );
    }

}




/**
 * Load the custom template for the current post.
 * @param  string $template Which template to load.
 * @return none
 */
function jold_cpt_templates_loader( $template ) {

    global $wp_query;
    $post = $wp_query->get_queried_object();

    if ($post) {
        $post_template = get_post_meta($post->ID,'_post_template',true);

        if (!empty($post_template) && $post_template!='default') {
            $template = get_stylesheet_directory() . "/{$post_template}";
        }
    }

    return $template;
}
